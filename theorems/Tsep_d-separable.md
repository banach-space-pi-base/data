---
uid: Tsep_d-separable
if:
  Psep_d: true
then:
  Pseparable: true
---

A predual of a separable Banach space is separable.
