---
uid: Tsomewhat_reflexive-pelczynski
if:
  Psomewhat_reflexive: true
then:
  Ppelczynski: true
---

$c_0$ is minimal, so its reflexive subspace must contain $c_0$, which cannot be reflexive.
