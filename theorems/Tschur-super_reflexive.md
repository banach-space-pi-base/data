---
uid: Tschur-super_reflexive
if:
  Pschur: true
then:
  Psuper_reflexive: false
---

A Schur Banach space cannot contain a reflexive infinite-dimensional space.
