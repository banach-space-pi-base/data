---
uid: Tseparable-grothendieck-reflexive
if:
  and:
    - Pseparable: true
    - Pgrothendieck: true
then:
  Preflexive: true
---

A separable Grothendieck space is reflexive.
