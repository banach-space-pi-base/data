---
uid: Tell_2_saturated-super_ell_p
if:
  and:
  - Pell_2_saturated: true
  - Pfindim: false
then:
  Psuper_ell_p: true
---
Trivial.
