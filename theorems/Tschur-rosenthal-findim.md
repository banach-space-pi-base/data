---
uid: Tschur-rosenthal-findim
if:
  and:
  - Pschur: true
  - Prosenthal: true
then:
  Pfindim: true
---

A Schur and Rosenthal Banach space is finite-dimensional.
