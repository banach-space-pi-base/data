---
uid: Tseparable-reflexive-sep_d
if:
  and:
    - Pseparable: true
    - Preflexive: true
then:
  Psep_d: true
---

Trivial.
