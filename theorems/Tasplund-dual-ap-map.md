---
uid: Tasplund-dual-ap-map
if:
  and:
    - Pasplund: true
    - Pdual: true
    - Pap: true
then:
  Pmap: true
---

In a dual Asplund space AP implies MAP.