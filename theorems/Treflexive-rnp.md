---
uid: Treflexive-rnp
if:
  Preflexive: true
then:
  Prnp: true
---

A reflexive Banach space has the Radon-Nikodym property.
