---
uid: Tsuper_unc_basis-rosenthal-pelczynski-super_reflexive
if:
  and:
    - Psuper_unc_basis: true
    - Prosenthal: true
    - Ppelczynski: true
then:
  Psuper_reflexive: true
---

A restatement of [a theorem of James](./Tunc_basis-rosenthal-pelczynski-reflexive).
