---
uid: TRlattice-rosenthal-asplund
if:
  and:
    - PRlattice: true
    - Prosenthal: true
then:
  Pasplund: true
refs:
  - doi: 10.1007/978-3-642-76724-1
    name: Banach lattices
---

A Banach lattice not containing an isomorphic copy of $\ell_1$ is Asplund.

This is a theorem due to Lotz, see, e.g., Theorem 5.4.14 of {{doi:10.1007/978-3-642-76724-1}}.
