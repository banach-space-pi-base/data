---
uid: Tbasis-separable
if:
  Pbasis: true
then:
  Pseparable: true
---

A Banach space with a basis is separable.
