---
uid: Tfindim-separable
if:
  Pfindim: true
then:
  Pseparable: true
---

A finite-dimensional Banach space is separable.
