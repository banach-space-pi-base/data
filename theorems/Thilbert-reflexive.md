---
uid: Thilbert-reflexive
if:
  Philbert: true
then:
  Preflexive: true
---

A Hilbert space is reflexive.
