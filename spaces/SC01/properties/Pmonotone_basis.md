---
uid: TC01-monotone_basis
space: SC01
property: Pmonotone_basis
value: true
---
$C[0,1]$ has a monotone basis.