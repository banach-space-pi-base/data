---
uid: Sell_2c
slug: ell_2c
name: $\ell_2(c)$
aliases:
  - Hilbert space of continuum
---
A Hilbert space of continuum.