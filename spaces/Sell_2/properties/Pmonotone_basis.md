---
uid: Tell_2-monotone_basis
space: Sell_2
property: Pmonotone_basis
value: true
---
$\ell_2$ has a monotone basis