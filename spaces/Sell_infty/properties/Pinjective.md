---
uid: Tell_infty-injective
space: Sell_infty
property: Pinjective
value: true
---

$\ell_\infty$ is injective.