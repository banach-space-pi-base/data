---
uid: Tell_infty-rosenthal
space: Sell_infty
property: Prosenthal
value: false
---

$\ell_\infty$ even contains $\ell_1(c)$ isometrically.