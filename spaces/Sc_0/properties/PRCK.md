---
uid: Tc_0-RCK
space: Sc_0
property: PRCK
value: true
---
$c_0 \simeq c = C(\mathbb N \cup \{\infty\})$