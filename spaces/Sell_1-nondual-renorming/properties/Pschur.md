---
uid: Tell_1-nondual-renorming-schur
space: Sell_1-nondual-renorming
property: Pschur
value: true
---
Schur is stable under isomorphisms.