---
uid: Tjt-ell_2_saturated
space: Sjt
property: Pell_2_saturated
value: true
---
JT is $\ell_2$-saturated.