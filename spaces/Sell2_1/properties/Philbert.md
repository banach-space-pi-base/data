---
uid: Tell2_1-hilbert
space: Sell2_1
property: Philbert
value: false
---

$\ell^2_1$ is not a Hilbert space