---
uid: Ttsirelson_T
space: Stsirelson_T
property: Psuper_ell_p
value: false
---
Tsirelson space contains no isomorphic copy of $\ell_p$, for any $p \in [1,\infty)$.