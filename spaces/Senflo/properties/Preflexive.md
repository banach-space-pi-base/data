---
uid: Tenflo-reflexive
space: Senflo
property: Preflexive
value: true
---
This Enflo space is reflexive.