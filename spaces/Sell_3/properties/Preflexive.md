---
uid: Tell_3-reflexive
space: Sell_3
property: Preflexive
value: true
---

$\ell_3$ is reflexive