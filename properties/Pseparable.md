---
uid: Pseparable
slug: separable
name: Separable
---

A separable Banach space. Separable spaces form an injective and surjective space ideal.