---
uid: Prosenthal
slug: rosenthal
name: Rosenthal
refs:
  - doi: 10.1073/pnas.71.6.2411
    name: A Characterization of Banach Spaces Containing $\ell_1$, by H.P. Rosenthal
  - doi: 10.4064/sm-95-1-1-15
    name: Metric characterization of first Baire class linear forms and octahedral norms, by G. Godefroy
---

A Banach space containing no isomorphic copy of $\ell_1$, or equivalently, having a weakly precompact unit ball: every bounded sequence contains a weakly Cauchy subsequence. Such spaces form the injective space ideal Space$(\mathcal{V}^{-1} \circ \mathcal{K})$.

Equivalently, it cannot be renormed to be octahedral.

The name "Rosenthal" seems to be settled upon just for the corresponding operators in $\mathcal{V}^{-1} \circ \mathcal{K}$ (see 3.2.4 in Operator Ideals). But it is convenient to also (ab)use it here for the spaces.  

AKA: $\neg$Super<$\ell_1$>

AKA: $\neg<$OH$>$