---
uid: Pell_2_saturated
slug: ell_2_saturated
name: $\ell_2$-saturated
---

A Banach space such that every its closed infinite-dimensional subspace contains an isomorphic copy of $\ell_2$.