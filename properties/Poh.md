---
uid: Poh
slug: oh
name: OH
aliases:
  - octahedral
---

An octahedral Banach space.