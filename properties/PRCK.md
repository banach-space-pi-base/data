---
uid: PRCK
slug: RCK
name: $<C(K)>$
---

A Banach space isomorphic to some $C(K)$.