---
uid: PCK
slug: CK
name: $C(K)$
---

A Banach space of continuous functions on some compact Hausdorff $K$ (up to isometry).