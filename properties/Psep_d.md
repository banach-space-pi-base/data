---
uid: Psep_d
slug: sep_d
name: Sep$^d$
---

A Banach space with a separable dual. This is an injective and surjective space ideal.